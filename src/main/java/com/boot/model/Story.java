package com.boot.model;

public class Story {

	private String storyName;
	private String date;
	private String storyDescription;
	private String storyOwner;
	private String sprintNo;
	private String key;

	public String getStoryName() {
		return storyName;
	}

	public String getDate() {
		return date;
	}

	public String getStoryDescription() {
		return storyDescription;
	}

	public String getStoryOwner() {
		return storyOwner;
	}

	public String getSprintNo() {
		return sprintNo;
	}

	public void setStoryName(String storyName) {
		this.storyName = storyName;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setStoryDescription(String storyDescription) {
		this.storyDescription = storyDescription;
	}

	public void setStroyOwner(String storyOwner) {
		this.storyOwner = storyOwner;
	}

	public void setSprintNo(String sprintNo) {
		this.sprintNo = sprintNo;
	}

	public void setStoryOwner(String storyOwner) {
		this.storyOwner = storyOwner;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}