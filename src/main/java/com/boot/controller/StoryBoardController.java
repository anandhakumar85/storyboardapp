package com.boot.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.boot.model.Story;

@Controller
public class StoryBoardController {

	private List<Story> storyList = new ArrayList<Story>();

	@RequestMapping("/read")
	public String home(Model model) {
		model.addAttribute("story", new Story());
		model.addAttribute("stories", this.storyList);
		return "story";
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String add(@ModelAttribute Story story, Model model) {
		if (story.getKey().equals("")) {
			this.storyList.add(story);
		} else {
			if (this.storyList.size() > 0) {
				this.storyList.remove(Integer.parseInt(story.getKey()));
				this.storyList.add(Integer.parseInt(story.getKey()), story);
			} else {
				this.storyList.add(story);
			}
		}
		model.addAttribute("stories", this.storyList);
		model.addAttribute("story", new Story());
		return "story";
	}

	@RequestMapping(value = "/edit/{index}", method = RequestMethod.POST)
	public String edit(@ModelAttribute Story story, Model model, @PathVariable("index") String index) {
		story = this.storyList.get(Integer.parseInt(index));
		story.setKey(index);
		model.addAttribute("story", story);
		model.addAttribute("stories", this.storyList);
		return "story";
	}

	@RequestMapping(value = "/delete/{index}", method = RequestMethod.POST)
	public String delete(@ModelAttribute Story story, Model model, @PathVariable("index") String index) {
		this.storyList.remove(Integer.parseInt(index));
		model.addAttribute("stories", this.storyList);
		return "story";
	}
}
