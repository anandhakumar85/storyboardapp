<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<title>Story Board</title>
</head>
<body>
	<form:form id="storyFrm" modelAttribute="story" action="/"
		method="post">
		<p class="p-4 mb-4 bg-danger text-white font-weight-bold">SPRING BOOT -> GIT LAB -> JENKIN [SAMPLE AGILE STORY BOARD APP]</p>
		<div class="form-group" style="width:700px">
			<form:input path="storyName" class="form-control"
				placeholder="Enter Story Name" />
			<form:input path="date" class="form-control"
				placeholder="Enter Target Completion Date" />
			<form:textarea path="storyDescription" class="form-control"
				placeholder="Enter Story Description" />
			<form:input path="storyOwner" class="form-control"
				placeholder="Enter Story Owner" />
			<form:input path="sprintNo" class="form-control"
				placeholder="Enter Sprint #" />
			<form:hidden path="key"/>
			<form:button name="Add" id="Add" onclick="submitForm('create', '')"
				class="btn btn-primary">Add/Save</form:button>
		</div>

		<div>
			<table class="table table-striped table-dark">
			<c:if test="${fn:length(stories) > 0}">
				<thead>
					<tr>
						<th scope="col">Story Name</th>
						<th scope="col">Target Date</th>
						<th scope="col">Story Description</th>
						<th scope="col">Story Owner</th>
						<th scope="col">Sprint #</th>
						<th scope="col">Edit Story</th>
						<th scope="col">Delete Story</th>
					</tr>
				</thead>
				</c:if>
				<tbody>
					<c:forEach items="${stories}" var="story" varStatus="count">
						<tr>
							<th scope="row">${story.storyName}</th>
							<td>${story.date}</td>
							<td>${story.storyDescription}</td>
							<td>${story.storyOwner}</td>
							<td>${story.sprintNo}</td>
							<td><form:button name="Edit"
									onclick="submitForm('edit','${count.index}')"
									class="btn btn-warning">Edit</form:button></td>
							<td><form:button name="Delete"
									onclick="submitForm('delete', '${count.index}')"
									class="btn btn-danger">Delete</form:button></td>
									
						</tr>
					</c:forEach>
				</tbody>
			</table>


		</div>
	</form:form>




	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous">
		
	</script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>

	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
	<script>
		function submitForm(val, index) {
			var frm = document.getElementById('storyFrm');
			frm.action = "/story/" + val + (index != '' ? ("/" + index) : '');
			if (val == 'edit') {
				document.getElementById('Save').style.visibility = "visible";
				document.getElementById('Add').style.visibility = "hidden";
			}
			frm.submit();
		}
	</script>
</body>
</html>